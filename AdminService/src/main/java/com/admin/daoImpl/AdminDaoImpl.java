package com.admin.daoImpl;

import java.util.Arrays;
import java.util.List;

import org.springframework.stereotype.Repository;

import com.admin.dao.AdminDao;
import com.admin.entity.Theatre;

@Repository
public class AdminDaoImpl implements AdminDao {

	@Override
	public Theatre addTheatre(Theatre theatre) {
		// save theatre to the database
		return new Theatre();
	}

	@Override
	public Theatre updateTheatre(Theatre theatre) {
		// update theatre in the database
		return new Theatre();
	}

	@Override
	public List<Theatre> getAllTheatres() {
		//fetch all list of theatre
		return Arrays.asList(new Theatre(), new Theatre());
	}

	@Override
	public Theatre getTheatreById(int theatreId) {
		// get theatre by from database using theatre id
		return new Theatre();
	}

	@Override
	public boolean deleteTheatre(int theatreId) {

		//delete theatre by id from database
		return true;
	}

}
