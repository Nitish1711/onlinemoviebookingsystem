package com.admin.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.admin.entity.Theatre;
import com.admin.service.AdminService;

@RestController
@Component
public class AdminController {

	@Autowired
	private AdminService adminService;

	// adding a theatre
	@PostMapping("/admin/theatre")
	@ResponseStatus
	public Theatre addTheatre(Theatre theatre) {
		return adminService.addTheatre(theatre);
	}

	@PutMapping("/admin/theatre")
	@ResponseStatus
	public Theatre updateTheatre(Theatre theatre) {
		return adminService.updateTheatre(theatre);
	}

	@GetMapping("/admin/theatre")
	@ResponseStatus
	public List<Theatre> getAllTheatres() {
		return adminService.getAllTheatres();
	}
	
	@DeleteMapping("/admin/theatre/{theatreId}")
	@ResponseStatus
	public boolean deleteTheatre(@PathVariable int theatreId) {
		return adminService.deleteTheatre(theatreId);
	}
	
	
	@GetMapping("/admin/theatre/{theatreId}")
	@ResponseStatus
	public Theatre getTheatreById(@PathVariable int theatreId) {
		return adminService.getTheatreById(theatreId);
	}



}
