package com.admin.entity;

import java.util.Date;
import java.util.List;

import com.admin.model.Address;
import com.admin.model.HallSeat;

public class Theatre {

	private int theatreId;

	private String theatreName;

	private Address address;

	private int totalSeats;

	private List<HallSeat> hallSeats;

	private Date bookedOn;
	
	private Movie movie;
	
	public Theatre() {
		// TODO Auto-generated constructor stub
	}

	public Theatre(int theatreId, Address address, int totalSeats, String theatreName, List<HallSeat> hallSeats) {
		super();
		this.theatreId = theatreId;
		this.address = address;
		this.totalSeats = totalSeats;
		this.theatreName = theatreName;
		this.hallSeats = hallSeats;
	}

	public int getTheatreId() {
		return theatreId;
	}

	public Address getAddress() {
		return address;
	}

	public int getTotalSeats() {
		return totalSeats;
	}

	public List<HallSeat> getHallSeats() {
		return hallSeats;
	}

	public String getTheatreName() {
		return theatreName;
	}

	public Date getBookedOn() {
		return bookedOn;
	}

	public void setBookedOn(Date bookedOn) {
		this.bookedOn = bookedOn;
	}

	public Movie getMovie() {
		return movie;
	}

	public void setMovie(Movie movie) {
		this.movie = movie;
	}

}
