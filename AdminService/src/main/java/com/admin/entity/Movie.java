/**
 * 
 */
package com.admin.entity;

import java.util.Date;
import java.util.List;

 
public class Movie {

	private int movieId;

	private String title;

	private String description;

	private int duration;

	private Date releaseDate;

	private List<Theatre> theatres; // one to many as a movie can be screened at more than one theatre

	public Movie(int movieId, String title, String description, int duration, Date releaseDate,List<Theatre> theatres) {
		this.movieId = movieId;
		this.title = title;
		this.description = description;
		this.duration = duration;
		this.releaseDate = releaseDate;
		this.theatres=theatres;
	}

	public int getMovieId() {
		return movieId;
	}

	public String getTitle() {
		return title;
	}

	public String getDescription() {
		return description;
	}

	public int getDuration() {
		return duration;
	}

	public Date getReleaseDate() {
		return releaseDate;
	}

	public List<Theatre> getTheatres() {
		return theatres;
	}

}
