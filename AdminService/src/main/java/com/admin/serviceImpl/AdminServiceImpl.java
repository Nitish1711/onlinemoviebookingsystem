package com.admin.serviceImpl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.admin.dao.AdminDao;
import com.admin.entity.Theatre;
import com.admin.service.AdminService;

@Service
public class AdminServiceImpl implements AdminService {

	@Autowired
	AdminDao adminDao;

	@Autowired
	RestTemplate restTemplate;

	@Override
	public Theatre addTheatre(Theatre theatre) {
		return adminDao.addTheatre(theatre);
	}

	@Override
	public Theatre updateTheatre(Theatre theatre) {
		return adminDao.updateTheatre(theatre);
	}

	@Override
	public List<Theatre> getAllTheatres() {
		return adminDao.getAllTheatres();
	}

	@Override
	public boolean deleteTheatre(int theatreId) {
		Theatre theatre = adminDao.getTheatreById(theatreId);
		if (theatre.getMovie() == null) {
			return adminDao.deleteTheatre(theatreId);
		}
		return false;
	}

	@Override
	public Theatre getTheatreById(int theatreId) {
		return adminDao.getTheatreById(theatreId);
	}

}
