package com.admin.model;

public class HallSeat {

	private int id;
	private boolean reserved;
	
	private double price;

	public HallSeat(int id, boolean reserved, double price) {
		super();
		this.id = id;
		this.reserved = reserved;
		this.price = price;
	}

	public int getId() {
		return id;
	}

	public boolean isReserved() {
		return reserved;
	}

	public double getPrice() {
		return price;
	}

}
