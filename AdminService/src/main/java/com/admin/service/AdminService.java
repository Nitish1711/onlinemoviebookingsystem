package com.admin.service;

import java.util.List;

import com.admin.entity.Theatre;

public interface AdminService {

	public Theatre addTheatre(Theatre theatre);

	public Theatre updateTheatre(Theatre theatre);

	public List<Theatre> getAllTheatres();

	public boolean deleteTheatre(int theatreId);

	public Theatre getTheatreById(int theatreId);

}
