package com.admin.dao;

import java.util.List;

import com.admin.entity.Theatre;

public interface AdminDao {

	public Theatre addTheatre(Theatre theatre);

	public Theatre updateTheatre(Theatre theatre);

	public List<Theatre> getAllTheatres();
	
	public Theatre getTheatreById(int theatreId);

	public boolean deleteTheatre(int theatreId);

}
