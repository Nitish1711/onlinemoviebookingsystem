package com.admin.service.test;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Matchers;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import com.admin.daoImpl.AdminDaoImpl;
import com.admin.entity.Theatre;
import com.admin.model.Address;
import com.admin.serviceImpl.AdminServiceImpl;

public class AdminServiceTest {

	@InjectMocks
	private AdminServiceImpl adminService;

	@Mock
	private AdminDaoImpl adminDao;

	private Theatre theatre;

	@Before
	public void runBeforeTestMethod() {
		MockitoAnnotations.initMocks(this);
		theatre = new Theatre(1, addAdress(), 60, "Regal", null);
	}

	private Address addAdress() {
		Address address = new Address();
		address.setCity("New Delhi");
		address.setStreetName("Golf course");
		return null;
	}

	@Test
	public void addTheatre() {
		Mockito.when(adminDao.addTheatre(Matchers.any(Theatre.class))).thenReturn(theatre);
		adminService.addTheatre(theatre);
	}

	@Test
	public void updateTheatre() {
		Theatre theatreNew = new Theatre(1, addAdress(), 60, "PVR", null);
		Mockito.when(adminDao.updateTheatre(Matchers.any(Theatre.class))).thenReturn(theatreNew);
		adminService.addTheatre(theatreNew);
	}

	@Test
	public void getAllTheatres() {
		Theatre theatre1 = new Theatre(1, addAdress(), 60, "PVR", null);
		Theatre theatre2 = new Theatre(1, addAdress(), 60, "Regal", null);
		List<Theatre> theatres = new ArrayList<Theatre>();
		theatres.add(theatre1);
		theatres.add(theatre2);

		Mockito.when(adminDao.getAllTheatres()).thenReturn(theatres);
		adminService.getAllTheatres();
	}

	@Test
	public void deleteTheatre() {
		Theatre theatre1 = new Theatre(1, addAdress(), 60, "PVR", null);
		theatre1.setMovie(null);
		Mockito.when(adminDao.getTheatreById(Matchers.anyInt())).thenReturn(theatre1);

		Mockito.when(adminDao.deleteTheatre(Matchers.anyInt())).thenReturn(true);
		adminService.deleteTheatre(theatre1.getTheatreId());
	}

}
