package com.search.service;

import java.util.List;
import com.search.entity.Movie;


public interface SearchService {
	
	 List<Movie> searchAllMovies();

}
