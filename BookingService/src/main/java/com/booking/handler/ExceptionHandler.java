package com.booking.handler;

import org.springframework.web.bind.annotation.ControllerAdvice;

import com.booking.exception.BookingUnusccessFulException;
import com.booking.exception.ValidationException;
import com.booking.model.BookingResponse;
import com.booking.model.Errors;

@ControllerAdvice
public class ExceptionHandler {

	@org.springframework.web.bind.annotation.ExceptionHandler(ValidationException.class)
	public BookingResponse handleValidationException(Exception e) {
		BookingResponse bookingResponse = new BookingResponse();
		bookingResponse.setError(populatError(e.getMessage(), "Maximum seats booking limit exceded per transaction"));
		return bookingResponse;
	}

	@org.springframework.web.bind.annotation.ExceptionHandler(BookingUnusccessFulException.class)
	public BookingResponse handleUnsucessFulBookingException(Exception e) {
		BookingResponse bookingResponse = new BookingResponse();
		bookingResponse.setError(populatError(e.getMessage(), "Slots chosen are not available"));
		return bookingResponse;
	}

	private Errors populatError(String message, String description) {
		Errors error = new Errors();
		error.setError(message);
		error.setDescription(description);
		return error;
	}

}
