package com.booking.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.booking.entity.Booking;
import com.booking.model.BookingResponse;
import com.booking.service.BookingService;

@RestController
@Component
public class BookingController {

	@Autowired
	private BookingService bookingService;

	@PostMapping
	@ResponseStatus
	public BookingResponse bookMovieTicket(Booking booking) {
		return bookingService.bookTicket(booking);
	}

}
