package com.booking.model;

import java.util.Date;

import com.booking.enumeration.PaymentStatus;

public abstract class Payment {
	public double amount;
	public Date createdOn;
	public int transactionId;
	public PaymentStatus status;
	
	public abstract boolean makePayment();


	public double getAmount() {
		return amount;
	}

	public void setAmount(double amount) {
		this.amount = amount;
	}

	public Date getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	public int getTransactionId() {
		return transactionId;
	}

	public void setTransactionId(int transactionId) {
		this.transactionId = transactionId;
	}

	public PaymentStatus getStatus() {
		return status;
	}

	public void setStatus(PaymentStatus status) {
		this.status = status;
	}
}
