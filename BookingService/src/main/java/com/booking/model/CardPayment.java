package com.booking.model;

import java.net.MalformedURLException;
import java.net.URL;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

public class CardPayment extends Payment {

	@Autowired
	RestTemplate restTemplate;

	private String cardNo;

	private int CVV;

	public CardPayment(String cardNo, int cVV, int amount) {
		super();
		this.cardNo = cardNo;
		this.CVV = cVV;
		this.amount = amount;
	}

	public String getCardNo() {
		return cardNo;
	}

	public int getCVV() {
		return CVV;
	}

	@Override
	public boolean makePayment() {
		// make call to online payment gateway
		boolean paymentSuccessful = false;
		
		//call to 3rd party payment gatway like just pay as below 
			//paymentSuccessful = restTemplate.getForObject(new URL("payment gateway url").toString(), null); 

		return true;
	}

}
