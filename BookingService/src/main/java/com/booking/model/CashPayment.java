package com.booking.model;

public class CashPayment extends Payment {

	private int totalCashCollected;

	public int getTotalCashCollected() {
		return totalCashCollected;
	}

	public void setTotalCashCollected(int totalCashCollected) {
		this.totalCashCollected = totalCashCollected;
	}

	@Override
	public boolean makePayment() {
		System.out.println("collect cash from customer on the booking date");
		return true;
	}

}
