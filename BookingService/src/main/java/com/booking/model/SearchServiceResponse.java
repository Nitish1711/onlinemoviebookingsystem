package com.booking.model;

import java.util.List;

public class SearchServiceResponse {
	
	private List<Integer> seatNumbers;

	public List<Integer> getSeatNumbers() {
		return seatNumbers;
	}

	public void setSeatNumbers(List<Integer> seatNumbers) {
		this.seatNumbers = seatNumbers;
	}

}
