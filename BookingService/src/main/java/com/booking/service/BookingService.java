package com.booking.service;

import com.booking.entity.Booking;
import com.booking.model.BookingResponse;

public interface BookingService {
	
	public BookingResponse bookTicket(Booking booking);

}
