package com.booking.daoImpl;

import org.springframework.stereotype.Repository;

import com.booking.dao.BookingDao;
import com.booking.entity.Booking;
import com.booking.enumeration.BookingStatus;
import com.booking.model.BookingResponse;

@Repository
public class BookingDaoImpl implements BookingDao {

	@Override
	public BookingResponse bookMovieTicket(Booking booking) {
		booking.setBookingStatus(BookingStatus.BOOKED);
		// save booking class in the database and make entry in user class for a booking
		booking.setBookingStatus(BookingStatus.BOOKED);
		BookingResponse response = new BookingResponse();
		// prepare response
		response.setBookingNumber(2333);
		return response;
	}

}
