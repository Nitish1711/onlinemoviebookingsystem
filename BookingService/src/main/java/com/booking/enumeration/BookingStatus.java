package com.booking.enumeration;

public enum BookingStatus {

	BOOKED, REJECTED, FAILED, PROCESSING

}
