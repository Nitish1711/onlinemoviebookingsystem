package com.booking.dao;

import com.booking.entity.Booking;
import com.booking.model.BookingResponse;

public interface BookingDao {

	public BookingResponse bookMovieTicket(Booking booking);
}
