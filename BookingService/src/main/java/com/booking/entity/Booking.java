package com.booking.entity;

import java.io.Serializable;
import java.util.List;

import com.booking.enumeration.BookingStatus;
import com.booking.model.Payment;

public class Booking implements Serializable {

	private static final long serialVersionUID = 1L;

	private User user;

	private String movieName;

	private int movieId;

	private int theatreId;

	private int numberOfSeats;
	
	private int amount;
	
	private boolean onlinePayment;


	private Payment payment;

	private BookingStatus bookingStatus;

	private List<Integer> chosenSeatIds; // one to many

	public String getMovieName() {
		return movieName;
	}

	public void setMovieName(String movieName) {
		this.movieName = movieName;
	}

	public int getMovieId() {
		return movieId;
	}

	public void setMovieId(int movieId) {
		this.movieId = movieId;
	}

	public int getTheatreId() {
		return theatreId;
	}

	public void setTheatreId(int theatreId) {
		this.theatreId = theatreId;
	}

	public int getNumberOfSeats() {
		return numberOfSeats;
	}

	public void setNumberOfSeats(int numberOfSeats) {
		this.numberOfSeats = numberOfSeats;
	}

	public BookingStatus getBookingStatus() {
		return bookingStatus;
	}

	public void setBookingStatus(BookingStatus bookingStatus) {
		this.bookingStatus = bookingStatus;
	}

	public List<Integer> getChosenSeatIds() {
		return chosenSeatIds;
	}

	public void setChosenSeatIds(List<Integer> chosenSeatIds) {
		this.chosenSeatIds = chosenSeatIds;
	}

	public Payment getPayment() {
		return payment;
	}

	public void setPayment(Payment payment) {
		this.payment = payment;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public int getAmount() {
		return amount;
	}

	public void setAmount(int amount) {
		this.amount = amount;
	}

	public boolean isOnlinePayment() {
		return onlinePayment;
	}

	public void setOnlinePayment(boolean onlinePayment) {
		this.onlinePayment = onlinePayment;
	}
}
