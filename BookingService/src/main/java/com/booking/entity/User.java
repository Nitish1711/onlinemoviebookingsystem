package com.booking.entity;

import java.util.List;

import com.booking.model.Person;

public class User extends Person {

	private List<Integer> bookings;

	public List<Integer> getBookings() {
		return bookings;
	}

	public void setBookings(List<Integer> bookings) {
		this.bookings = bookings;
	}

}
