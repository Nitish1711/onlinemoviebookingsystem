package com.booking.serviceImpl;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import com.booking.dao.BookingDao;
import com.booking.entity.Booking;
import com.booking.enumeration.BookingStatus;
import com.booking.exception.BookingUnusccessFulException;
import com.booking.exception.PaymentFailureException;
import com.booking.exception.ValidationException;
import com.booking.model.BookingResponse;
import com.booking.model.CardPayment;
import com.booking.model.Payment;
import com.booking.model.SearchServiceResponse;
import com.booking.service.BookingService;

@Service
public class BookingServiceImpl implements BookingService {

	@Autowired
	BookingDao bookingDao;

	@Autowired
	RestTemplate restTemplate;

	@Override
	public BookingResponse bookTicket(Booking booking) {

		Payment p = null;
		boolean paymentSuccess = false;

		if (booking.getNumberOfSeats() > 9) {
			booking.setBookingStatus(BookingStatus.REJECTED);
			throw new ValidationException("LIMIT_EXCEEDED");
		}

		// consuming search microservice to get available slots

		try {
			SearchServiceResponse searchResponse = restTemplate.getForObject(
					new URL("http://localhost:8080/search/slots/" + booking.getMovieId() + "/" + booking.getTheatreId())
							.toString() + ")",
					SearchServiceResponse.class);
			List<Integer> seatsAvailable = searchResponse.getSeatNumbers();
			boolean slotsAvailable = booking.getChosenSeatIds().stream()
					.allMatch(seat -> seatsAvailable.contains(seat));
			if (!slotsAvailable) {
				throw new BookingUnusccessFulException("SLOTS_UNAVAILABLE");
			}

			if (booking.isOnlinePayment()) {
				p = new CardPayment(booking.getUser().getCardNo(), booking.getUser().getCVV(), booking.getAmount());
				paymentSuccess = p.makePayment();
			}
		} catch (RestClientException e) {
			e.printStackTrace();
		} catch (MalformedURLException e) {
			e.printStackTrace();
		}

		if (!paymentSuccess && booking.isOnlinePayment()) {
			throw new PaymentFailureException("PAYMENT_FAILED");
		}
		return bookingDao.bookMovieTicket(booking);
	}

}
