package com.booking.exception;

public class PaymentFailureException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	public PaymentFailureException(String message) {
		super(message);
	}

}
