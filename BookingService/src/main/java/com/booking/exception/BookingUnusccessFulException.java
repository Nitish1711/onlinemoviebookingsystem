package com.booking.exception;

public class BookingUnusccessFulException extends RuntimeException {
	
	private static final long serialVersionUID = 1L;

	public BookingUnusccessFulException(String message) {
		super(message);
	}


}
