package com.booking.service.test;

import java.util.Arrays;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Matchers;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.web.client.RestTemplate;

import com.booking.daoImpl.BookingDaoImpl;
import com.booking.entity.Booking;
import com.booking.entity.User;
import com.booking.exception.ValidationException;
import com.booking.model.BookingResponse;
import com.booking.model.CardPayment;
import com.booking.model.SearchServiceResponse;
import com.booking.serviceImpl.BookingServiceImpl;

public class BookingServiceTest {

	@InjectMocks
	private BookingServiceImpl bookingService;

	@Mock
	private BookingDaoImpl bookingDao;

	@Mock
	private RestTemplate restTemplate;
	
	@Mock
	private CardPayment cardPayment;


	@Before
	public void runBeforeTestMethod() {
		MockitoAnnotations.initMocks(this);
	}

	@Test(expected = ValidationException.class)
	public void bookTicketWithSeatsSelectedExcededMaxLimit() {
		BookingResponse response = prepareBookingResponse();
		Booking booking = new Booking();
		booking.setNumberOfSeats(11);
		booking.setMovieId(12);
		Mockito.when(bookingDao.bookMovieTicket(Matchers.any(Booking.class))).thenReturn(response);
		bookingService.bookTicket(booking);
	}

	@Test
	public void bookTicketWithSucess() {
		BookingResponse response = prepareBookingResponse();
		Booking booking = new Booking();
		booking.setNumberOfSeats(2);
		booking.setMovieId(12);
		booking.setChosenSeatIds(Arrays.asList(1, 3, 4));
		booking.setUser(populateUser());
		booking.setAmount(150);
		booking.setOnlinePayment(true);

		SearchServiceResponse searchresp = new SearchServiceResponse();
		searchresp.setSeatNumbers(booking.getChosenSeatIds());
		Mockito.when(bookingDao.bookMovieTicket(Matchers.any(Booking.class))).thenReturn(response);
		Mockito.when(restTemplate.getForObject(Matchers.anyString(), Matchers.anyObject())).thenReturn(searchresp);
		Mockito.when(cardPayment.makePayment()).thenReturn(true);

		bookingService.bookTicket(booking);
	}

	private User populateUser() {
		User user = new User();
		user.setName("Nitish");
		user.setAccNo(12345);
		user.setCVV(223);
		user.setCardNo("2345566");
		return user;
	}

	private BookingResponse prepareBookingResponse() {
		BookingResponse response = new BookingResponse();
		response.setBookingNumber(2233);
		response.setMovieName("Bahubali");
		response.setUserId(22);
		response.setNumberOfSeats(3);
		response.setSeatNumbers(Arrays.asList(1, 3, 4));
		return response;
	}

}
