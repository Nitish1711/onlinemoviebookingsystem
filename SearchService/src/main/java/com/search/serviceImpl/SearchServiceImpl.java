package com.search.serviceImpl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.search.dao.SearchDao;
import com.search.entity.Movie;
import com.search.model.HallSeat;
import com.search.service.SearchService;

@Service
public class SearchServiceImpl implements SearchService {

	@Autowired
	SearchDao searchDao;

	@Override
	public List<Movie> searchAllMovies() {
		List<Movie> movies = searchDao.searchAllMovies();
		return movies;
	}

	@Override
	public List<HallSeat> searchAllAvailableSlots(int movieId, int theatreId) {
		List<HallSeat> availableSlots = searchDao.findAllAvailableSlots(movieId, theatreId);
		return availableSlots;
	}

}
