package com.search.daoImpl;

import java.util.Collections;
import java.util.List;

import org.springframework.stereotype.Repository;

import com.search.dao.SearchDao;
import com.search.entity.Movie;
import com.search.model.HallSeat;

@Repository
public class SearchDaoImpl implements SearchDao {

	@Override
	public List<Movie> searchAllMovies() {
		List<Movie> movies = Collections.emptyList();// will pick up this list from database
		return movies;
	}

	@Override
	public List<HallSeat> findAllAvailableSlots(int movieId, int theatreId) {
		List<HallSeat> availableSlots = Collections.emptyList();// will pick up this list from database
		return availableSlots;
	}
}
