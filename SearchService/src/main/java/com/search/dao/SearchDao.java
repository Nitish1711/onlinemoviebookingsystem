package com.search.dao;

import java.util.List;
import com.search.entity.Movie;
import com.search.model.HallSeat;


public interface SearchDao {
	
	 List<Movie> searchAllMovies();

	List<HallSeat> findAllAvailableSlots(int movieId, int theatreId);

}
