package com.search.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.search.entity.Movie;
import com.search.model.HallSeat;
import com.search.service.SearchService;

@RestController
@Component
public class SearchController {

	@Autowired
	private SearchService searchService;

	@GetMapping("/search/movies")
	@ResponseStatus
	public List<Movie> getEmployeePersonalInfo(@PathVariable int id) {
		System.out.println("getting personal info");
		return searchService.searchAllMovies();
	}

	@GetMapping("/search/slots/{movieId}/{theatreId}")
	@ResponseStatus
	public List<HallSeat> getAllAvailableSlotsOfTheatre(@PathVariable int movieId, @PathVariable int theatreId) {

		return searchService.searchAllAvailableSlots(movieId, theatreId);
	}

}
