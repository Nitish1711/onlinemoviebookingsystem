package com.search.service;

import java.util.List;
import com.search.entity.Movie;
import com.search.model.HallSeat;

public interface SearchService {

	List<Movie> searchAllMovies();

	List<HallSeat> searchAllAvailableSlots(int movieId, int theatreId);

}
