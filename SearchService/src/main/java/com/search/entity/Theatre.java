package com.search.entity;

import java.util.List;

import com.search.model.Address;
import com.search.model.HallSeat;

public class Theatre {

	private int theatreId;

	private String theatreName;

	private Address address;

	private int totalSeats;

	private List<HallSeat> hallSeats;

	public Theatre(int theatreId, Address address, int totalSeats, String theatreName, List<HallSeat> hallSeats) {
		super();
		this.theatreId = theatreId;
		this.address = address;
		this.totalSeats = totalSeats;
		this.theatreName = theatreName;
		this.hallSeats = hallSeats;
	}

	public int getTheatreId() {
		return theatreId;
	}

	public Address getAddress() {
		return address;
	}

	public int getTotalSeats() {
		return totalSeats;
	}

	public List<HallSeat> getHallSeats() {
		return hallSeats;
	}

	public String getTheatreName() {
		return theatreName;
	}

}
