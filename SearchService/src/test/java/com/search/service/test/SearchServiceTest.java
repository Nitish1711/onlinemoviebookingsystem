package com.search.service.test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Matchers;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import com.search.daoImpl.SearchDaoImpl;
import com.search.entity.Movie;
import com.search.entity.Theatre;
import com.search.model.Address;
import com.search.model.HallSeat;
import com.search.serviceImpl.SearchServiceImpl;

import junit.framework.Assert;

public class SearchServiceTest {

	@InjectMocks
	private SearchServiceImpl searchService;

	private List<Movie> movies;

	@Mock
	private SearchDaoImpl searchDao;

	@Before
	public void runBeforeTestMethod() {
		MockitoAnnotations.initMocks(this);
		movies = getMovies();
	}

	@Test
	public void searchAllMoviesWithTheatres() {
		Mockito.when(searchDao.searchAllMovies()).thenReturn(movies);
		List<Movie> movies = searchService.searchAllMovies();
		movies.forEach(movie -> {
			movie.getTheatres().forEach(theatre -> {
				System.out.println(movie.getTitle() + " running in " + theatre.getTheatreName());
			});
		});
	}

	@Test
	public void searchAllAvailablSlots() {

		List<HallSeat> hallSeats = mockAvaialbleSlots();
		Mockito.when(searchDao.findAllAvailableSlots(Matchers.anyInt(), Matchers.anyInt())).thenReturn(hallSeats);
		List<HallSeat> availableSeats = searchService.searchAllAvailableSlots(Matchers.anyInt(), Matchers.anyInt());
		Assert.assertEquals(false, availableSeats.get(0).isReserved());
	}

	private List<HallSeat> mockAvaialbleSlots() {
		List<HallSeat> hallSeats = new ArrayList<HallSeat>();
		Address address1 = new Address();
		address1.setCity("New Delhi");
		address1.setPlotNo(12);
		address1.setStreetName("Kailash Colony");
		address1.setZipCode(110075);

		Movie movie = new Movie(1, "Karate Kid", "Motivational", 100, new Date(), Arrays.asList(new Theatre(2, address1,
				30, "Regal", Arrays.asList(new HallSeat(1, false, 150), new HallSeat(2, true, 150)))));
		List<HallSeat> availableSeat = movie.getTheatres().get(0).getHallSeats().stream()
				.filter(hallSeat -> !hallSeat.isReserved()).collect(Collectors.toList());
		return availableSeat;
	}

	private List<Movie> getMovies() {

		List<Movie> movies = new ArrayList<Movie>();
		Address address1 = new Address();
		address1.setCity("New Delhi");
		address1.setPlotNo(12);
		address1.setStreetName("Kailash Colony");
		address1.setZipCode(110075);

		Address address2 = new Address();
		address1.setCity("New Delhi");
		address1.setPlotNo(12);
		address1.setStreetName("Nehru Place");
		address1.setZipCode(110075);

		Movie movie1 = new Movie(1, "Karate Kid", "Motivational", 100, new Date(),
				Arrays.asList(new Theatre(2, address1, 30, "Regal", Collections.emptyList()),
						new Theatre(2, address2, 20, "PVR", Collections.emptyList())));
		Movie movie2 = new Movie(1, "3 Idiots", "Drama", 150, new Date(),
				Arrays.asList(new Theatre(2, address1, 30, "Big", Collections.emptyList()),
						new Theatre(2, address2, 20, "TDI", Collections.emptyList())));

		movies.add(movie1);
		movies.add(movie2);

		return movies;
	}

}
